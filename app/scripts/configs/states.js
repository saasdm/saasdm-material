/* jslint todo: true */
/* jslint xxx: true */
/* jshint -W100 */
'use strict';

angular.module('saasdm-material')
/*
 * ماشین حالت نرم افزار
 */
.config(function($routeProvider) {
    $routeProvider//
    .when('/', {
	templateUrl : 'views/amh-content.html',
	controller : 'AmhContentCtrl',
    })//
    .when('/content/:name', {
	templateUrl : 'views/amh-content.html',
	controller : 'AmhContentCtrl'
    })//
    .when('/login', {
	templateUrl : 'views/amh-login.html',
	controller : 'LoginCtrl'
    })//
    .when('/signup', {
	templateUrl : 'views/amh-signup.html',
	controller : 'SignupCtrl'
    })//
    .when('/setting/google-analytic', {
	templateUrl : 'views/amh-setting-google-analytic.html',
//	controller : 'LoginCtrl'
    })//
    .when('/setting/brand', {
	templateUrl : 'views/amh-setting-brand.html',
//	controller : 'LoginCtrl'
    })//
    .otherwise({
	redirectTo : '/'
    });
});
