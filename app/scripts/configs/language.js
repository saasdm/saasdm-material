/* jslint todo: true */
/* jslint xxx: true */
/* jshint -W100 */
'use strict';

angular
.module('saasdm-material')

.config(
	function($translateProvider) {
	    $translateProvider
	    //
	    .translations(
		    'fa',
		    {
			'language' : 'زبان',
			'sign in' : 'ورود',
			'sign out' : 'خروج',
			'email' : 'رایانامه',
			'name' : 'نام',
			'phone' : 'شماره تماس',
			'home' : 'خانه',
			'close' : 'بستن',
			'ok' : 'تایید',
			'user login' : 'ورود کاربر',
			'user name' : 'نام کاربری',
			'password' : 'گذرواژه',
			'submit' : 'ثبت',
			'about us' : 'در مورد ما',
			'contact us' : 'تماس با ما',
			'blog' : 'بلاگ',

			'app.update.title' : 'به روز رسانی',
			'app.update.message' : 'نسخه  سایت در دسترس است، لطفا دوباره صفحه را باز کنید.',

			'loading ...' : 'در حال بارگزاری ...',

		    });
	    $translateProvider.preferredLanguage('fa');
	});
